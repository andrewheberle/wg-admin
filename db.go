package main

import (
	"fmt"
	"io/fs"

	bolt "go.etcd.io/bbolt"
)

func openDb(path string, mode fs.FileMode, options *bolt.Options) (*Db, error) {
	db, err := bolt.Open(path, 0600, nil)
	if err != nil {
		return &Db{}, err
	}
	if err := db.Update(func(tx *bolt.Tx) error {
		var err error
		_, err = tx.CreateBucketIfNotExists([]byte("users"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		_, err = tx.CreateBucketIfNotExists([]byte("keys"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	}); err != nil {
		return &Db{}, err
	}

	return &Db{db}, nil
}

// Close wrapper for Bolt DB
func (db *Db) Close() error {
	return db.kv.Close()
}

func (db *Db) View(fn func(*bolt.Tx) error) error {
	return db.kv.View(fn)
}

func (db *Db) Put(bucket, key, value []byte) error {
	return db.kv.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket)
		if err := b.Put(key, value); err != nil {
			return err
		}
		return nil
	})
}

func (db *Db) Get(bucket, key []byte) (v []byte, err error) {
	err = db.kv.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket)
		if b == nil {
			return fmt.Errorf("bucket does not exist")
		}
		v = b.Get(key)
		return nil
	})

	return
}

func (db *Db) Del(bucket, key []byte) (err error) {
	return
}
