package main

import (
	"context"
	"log"
	"net/http"
)

// authMiddleware checks the user is logged in by verifying the provided JWT
func authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var email string

		log.Printf("handling connection to: %s", r.URL.Path)
		// skip with dummy info if local mode is enabled
		if *localMode {
			email = "test@example.net"
		} else {
			// otherwise do full verify
			accessJWT := r.Header.Get("Cf-Access-Jwt-Assertion")
			if accessJWT == "" {
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}

			jwtCtx := context.TODO()
			_, err := verifier.Verify(jwtCtx, accessJWT)
			if err != nil {
				http.Error(w, "Invalid token", http.StatusUnauthorized)
				return
			}

			email = r.Header.Get("Cf-Access-Authenticated-User-Email")
			if email == "" {
				http.Error(w, "Bad Request", http.StatusBadRequest)
				return
			}
		}

		user, err := newUserFromEmail(email)
		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		ctx := newContextWithUser(r.Context(), user)

		// Call next handler/middleware
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// aclMiddleware checks if the user is allowed to access the resource
// this is messy and should be done better
func aclMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// get user info
		user := userFromContext(r.Context())

		// check for app routes
		if route, found := routes[r.URL.Path]; found {
			if route.NeedsSuperuser && !user.isSuperUser() {
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}
		}

		// check api locations
		switch r.URL.Path {
		case "/api/allkeys":
			if !user.isSuperUser() {
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}
		}

		next.ServeHTTP(w, r)
	})
}

// loadKeys grabs the users devices from the DB
func loadKeys(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/":
			{
				// get user info
				user := userFromContext(r.Context())
				ctx := user.newContextWithKeys(r.Context())

				next.ServeHTTP(w, r.WithContext(ctx))
			}
		case "/admin":
			{
				// get user info
				user := userFromContext(r.Context())
				ctx := user.newContextWithAllKeys(r.Context())

				next.ServeHTTP(w, r.WithContext(ctx))
			}
		default:
			{
				next.ServeHTTP(w, r)
			}
		}
	})
}

// setSecurityHeaders sets various security based headers
func setSecurityHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Security-Policy", "default-src 'self'; object-src 'none'; frame-src 'none'")
		w.Header().Set("X-Content-Type-Options", "nosniff")
		next.ServeHTTP(w, r)
	})
}
