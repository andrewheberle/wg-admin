package main

import (
	"fmt"
	"log"
	"net/http"
	"text/template"
)

type Route struct {
	Name           string
	Title          string
	Template       string
	NeedsSuperuser bool
}

var routes = map[string]Route{
	"/":      {"Home", "Activate", "home.tmpl", false},
	"/admin": {"Admin", "Admin", "admin.tmpl", true},
}

func mainApp(w http.ResponseWriter, r *http.Request) {
	route, found := routes[r.URL.Path]
	if !found {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}

	// get user info
	user := userFromContext(r.Context())
	keys := keysFromContext(r.Context())

	w.WriteHeader(http.StatusOK)
	t, _ := template.ParseFS(embeddedFS, "templates/base.tmpl", fmt.Sprintf("templates/%s", route.Template))

	data := struct {
		Title       string
		Name        string
		Pages       map[string]Route
		UserInfo    User
		Keys        []Key
		IsSuperuser bool
	}{"Wireguard Manager", route.Name, routes, user, keys, user.isSuperUser()}
	if err := t.ExecuteTemplate(w, "base", data); err != nil {
		log.Printf("Template error: %s", err)
	}
}
