package main

import "context"

type key int

const (
	userKey key = iota
	keyListKey
)

func newContextWithUser(ctx context.Context, user User) context.Context {
	return context.WithValue(ctx, userKey, user)
}

func userFromContext(ctx context.Context) User {
	return ctx.Value(userKey).(User)
}

func keysFromContext(ctx context.Context) []Key {
	return ctx.Value(keyListKey).([]Key)
}
