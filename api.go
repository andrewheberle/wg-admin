package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	contentTypeJson = "application/json; charset=utf-8"
)

func getUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", contentTypeJson)
	w.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(w)
	user := userFromContext(r.Context())
	if err := encoder.Encode(user); err != nil {
		_ = encoder.Encode(struct {
			Error string `json:"error"`
		}{fmt.Sprint(err)})
	}
}

func getUserCapability(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", contentTypeJson)
	w.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(w)
	user := userFromContext(r.Context())
	capability := struct {
		Superuser bool `json:"superuser"`
	}{
		Superuser: user.isSuperUser(),
	}
	if err := encoder.Encode(capability); err != nil {
		_ = encoder.Encode(Response{err.Error()})
	}
}

func addDevice(w http.ResponseWriter, r *http.Request) {

}

func deleteDevice(w http.ResponseWriter, r *http.Request) {
	var action Action
	var bucket = []byte("keys")

	// json encoder/decoder for later
	encoder := json.NewEncoder(w)
	decoder := json.NewDecoder(r.Body)

	// always send JSON
	w.Header().Set("Content-Type", contentTypeJson)

	// decode request body
	if err := decoder.Decode(&action); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_ = encoder.Encode(Response{err.Error()})
		return
	}

	// check the request is valid
	if action.Command != "delete" || action.Object != "device" {
		w.WriteHeader(http.StatusBadRequest)
		_ = encoder.Encode(Response{"invalid request"})
		return
	}

	// get device
	key, err := db.Get(bucket, action.Key)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_ = encoder.Encode(Response{err.Error()})
		return
	}

	if key == nil {
		w.WriteHeader(http.StatusNotFound)
		_ = encoder.Encode(Response{"device not found"})
		return
	}

	// check user is allowed to delete
	user := userFromContext(r.Context())
	allowed := user.isSuperUser()
	if !allowed {
		for _, v := range user.Keys {
			if bytes.Equal(v, key) {
				allowed = true
			}
		}
	}
	if !allowed {
		w.WriteHeader(http.StatusUnauthorized)
		_ = encoder.Encode(Response{"user not authorized"})
		return
	}

	// do delete
	if err := db.Del(bucket, key); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_ = encoder.Encode(Response{err.Error()})
		return
	}

	w.WriteHeader(http.StatusOK)
	_ = encoder.Encode(Response{"device deleted"})
}
