package main

import (
	"embed"
	"flag"
	"html/template"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/coreos/go-oidc"
	"github.com/gorilla/mux"
)

var (
	wgStartIP                                  = "10.69.0.0"
	wgAllowedIPs                               = "0.0.0.0/0, ::/0"
	dbPath                                     = filepath.Join(".", "data.db")
	wgEndpoint, cfAccessUri, cfAccessPolicyAUD string

	db *Db

	verifier *oidc.IDTokenVerifier

	//go:embed css js templates
	embeddedFS embed.FS

	// command line flags
	localMode = flag.Bool("local-mode", false, "Enable local testing mode")

	// templates
	templates *template.Template
)

func getFileSystem(embedded fs.FS, dir string) http.FileSystem {
	fsys, err := fs.Sub(embedded, dir)
	if err != nil {
		panic(err)
	}

	return http.FS(fsys)
}

func main() {
	var err error

	// parse command line flags
	flag.Parse()

	// get values from env vars
	if ip := os.Getenv("WIREGUARD_START_IP"); ip != "" {
		wgStartIP = ip
	}
	if allowed := os.Getenv("ALLOWED_IPS"); allowed != "" {
		wgAllowedIPs = allowed
	}
	if endpoint := os.Getenv("WIREGUARD_ENDPOINT"); endpoint != "" {
		wgEndpoint = endpoint
	} else {
		log.Fatal("WIREGUARD_ENDPOINT must be set")
	}
	if cf := os.Getenv("CF_ACCESS_URI"); cf != "" {
		cfAccessUri = cf
	} else {
		log.Fatal("CF_ACCESS_URI must be set")
	}
	if aud := os.Getenv("CF_ACCESS_POLICY_AUD"); aud != "" {
		cfAccessPolicyAUD = aud
	}
	if db := os.Getenv("DB_PATH"); db != "" {
		dbPath = db
	}

	// load templates
	templates, err = template.ParseFS(embeddedFS, "templates/*.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	// open and init db if required
	db, err = openDb(dbPath, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// create jwt verifier
	verifier = newVerifier(cfAccessUri, cfAccessPolicyAUD)

	// new router
	r := mux.NewRouter()
	r.Use(setSecurityHeaders)
	r.Use(authMiddleware)
	r.Use(aclMiddleware)
	r.Use(loadKeys)

	// api routes
	api := r.PathPrefix("/api").Subrouter()
	api.Path("/user").Methods("GET").HandlerFunc(getUser)
	api.Path("/capability").Methods("GET").HandlerFunc(getUserCapability)
	api.Path("/device").Methods("POST").HandlerFunc(addDevice)
	api.Path("/device").Methods("DELETE").HandlerFunc(deleteDevice)

	// main route
	r.PathPrefix("/css/").Handler(http.StripPrefix("/css/", http.FileServer(getFileSystem(embeddedFS, "css"))))
	r.PathPrefix("/js/").Handler(http.StripPrefix("/js/", http.FileServer(getFileSystem(embeddedFS, "js"))))
	r.PathPrefix("/").HandlerFunc(mainApp)

	// server object
	srv := &http.Server{
		Handler:      r,
		Addr:         "0.0.0.0:8080",
		WriteTimeout: 5 * time.Second,
		ReadTimeout:  5 * time.Second,
	}

	// run http server
	log.Fatal(srv.ListenAndServe())
}
