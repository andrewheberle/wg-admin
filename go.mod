module gitlab.com/andrewheberle/wg-admin

go 1.16

require (
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/pquerna/cachecontrol v0.1.0 // indirect
	go.etcd.io/bbolt v1.3.5
	golang.org/x/oauth2 v0.0.0-20210427180440-81ed05c6b58c // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
