package main

import (
	"context"
	"fmt"

	"github.com/coreos/go-oidc"
)

func newVerifier(uri, aud string) *oidc.IDTokenVerifier {
	var (
		ctx    = context.TODO()
		config = &oidc.Config{
			ClientID: aud,
		}
		certsURL = fmt.Sprintf("%s/cdn-cgi/access/certs", uri)
		keySet   = oidc.NewRemoteKeySet(ctx, certsURL)
	)

	return oidc.NewVerifier(uri, keySet, config)
}
