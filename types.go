package main

import (
	"context"
	"encoding/json"
	"log"
	"os"
	"strings"

	bolt "go.etcd.io/bbolt"
)

type Db struct {
	kv *bolt.DB
}

type User struct {
	Email string   `json:"email"`
	Name  string   `json:"name"`
	Keys  [][]byte `json:"keys"`
}

type Key struct {
	Id         []byte `json:"id"`
	DeviceName string `json:"name"`
	User       string `json:"user"`
	IsDeleted  bool   `json:"deleted"`
	IsDisabled bool   `json:"disabled"`
	IPAddress  string `json:"ip"`
}

type Action struct {
	Command string `json:"command"`
	Object  string `json:"object"`
	Key     []byte `json:"key"`
}

type Response struct {
	Message string `json:"message"`
}

func newUserFromEmail(email string) (user User, err error) {
	v, err := db.Get([]byte("users"), []byte(email))
	if err != nil {
		return User{}, err
	}

	// check if a value came back
	if v == nil {
		log.Print("user not found")
		if err := db.kv.Update(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte("users"))
			user = User{Email: email}
			v, err := json.Marshal(user)
			if err != nil {
				return err
			}
			if err := b.Put([]byte(email), v); err != nil {
				return err
			}
			return nil
		}); err != nil {
			return User{}, err
		}

		// return result
		return user, nil
	}

	log.Print("user found")

	// unmarshal json into User type
	if err := json.Unmarshal(v, &user); err != nil {
		return User{}, err
	}

	// return result
	return user, nil
}

func (user *User) isSuperUser() bool {
	superUserList := strings.Split(os.Getenv("SUPERUSER_LIST"), " ")
	for _, email := range superUserList {
		if user.Email == email {
			return true
		}
	}
	return false
}

func (user *User) getKeys(all bool) []Key {
	var key Key

	keys := make([]Key, 0)
	for _, v := range user.Keys {
		data, err := db.Get([]byte("keys"), v)
		if err != nil {
			continue
		}

		if err := json.Unmarshal(data, &key); err != nil {
			continue
		}

		keys = append(keys, key)
	}

	return keys
}

func (user *User) newContextWithKeys(ctx context.Context) context.Context {
	return context.WithValue(ctx, keyListKey, user.getKeys(false))
}

func (user *User) newContextWithAllKeys(ctx context.Context) context.Context {
	return context.WithValue(ctx, keyListKey, user.getKeys(true))
}
